#!/bin/bash

rm -rf _site
mkdir _site

rst2html --stylesheet=styles/styles_rst.css,styles/pygment_trac.css --link-stylesheet index.rst _site/index.html 

mkdir _site/styles/
cp styles/pygment_trac.css _site/styles/
cp styles/styles_rst.css _site/styles/
