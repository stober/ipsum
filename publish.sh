#!/bin/bash -x

TS=`date '+%Y-%m-%d_%H:%M:%S'`
CONFIG="site.conf" # configures TARGET 
. $CONFIG
rsync -rzv _site/ $TARGET --exclude archives --backup --delete --backup-dir=archives/$TS
rsync -v .htaccess $TARGET/archives/
